import json

import telebot

import config
import youtubedownloader

bot = telebot.TeleBot(config.TELETUBE_BOT_TOKEN)

with open(config.LOCALISATION) as localisation:
    bot_activity = json.loads(localisation.read())


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, bot_activity["en"]["commands"][message.text])


@bot.message_handler(content_types=['text'])
def send_video(message):
    video_id, video_title, path = youtubedownloader.download(message.text)
    bot.send_message(message.chat.id, video_title)
    with open(path, 'rb') as data:
        bot.send_video(message.chat.id, data)
