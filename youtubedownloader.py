import os

import pytube

import config


def download(url):
    yt = pytube.YouTube(url)
    query = yt.streams.filter(file_extension='mp4').first()
    download_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        config.VIDEOS_FOLDER
    )
    query.download(download_path)
    path = os.path.join(
        download_path,
        query.default_filename
    )
    return yt.video_id, yt.title, path


