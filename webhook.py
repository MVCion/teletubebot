import flask
import telebot

import config
import logger
import teletubebot

app = flask.Flask(__name__)
bot = teletubebot.bot


@app.route('/', methods=['GET', 'HEAD'])
def index():
    return ''


@app.route(config.WEBHOOK_URL_PATH, methods=['POST'])
def web_hook():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        logger.web_hook_logger.info(json_string)
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        logger.web_hook_logger.error(flask.request.headers.values())
        flask.abort(403)
