import time

import config
import teletubebot

if __name__ == '__main__':
    if config.SET_WEB_HOOK:
        import webhook

        teletubebot.bot.remove_webhook()
        time.time.sleep(1)
        try:
            teletubebot.bot.set_webhook(url=config.WEBHOOK_URL_BASE + config.WEBHOOK_URL_PATH,
                                        certificate=open(config.WEBHOOK_SSL_CERT, 'r'))
        except IOError as error:
            raise Exception("Invalid web hook SSL certificate")

        webhook.app.run(host=config.WEBHOOK_LISTEN,
                        port=config.WEBHOOK_PORT,
                        ssl_context=(config.WEBHOOK_SSL_CERT, config.WEBHOOK_SSL_PRIV),
                        debug=config.DEBUG)

    else:
        teletubebot.bot.polling(none_stop=True)
