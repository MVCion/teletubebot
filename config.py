DEBUG = True
SET_WEB_HOOK = False

LOCALISATION = 'langs_localisation.json'

LOGS_FOLDER = 'log'
VIDEOS_FOLDER = 'videos'

TELETUBE_BOT_TOKEN = '627580352:AAFLK2tS8f5pzFtG_eJHAiq1qpmSrHtUK5A'

WEBHOOK_HOST = '139.59.182.7'
WEBHOOK_PORT = 8443  # 443 / 80 / 88 / 8443 (must be opened)
WEBHOOK_LISTEN = '0.0.0.0'  # try '139.59.182.7'

WEBHOOK_SSL_CERT = '/root/webhook_cert.pem'  # path to certificate
WEBHOOK_SSL_PRIV = '/root/webhook_pkey.pem'  # path to private key

WEBHOOK_URL_BASE = 'https://%s:%s' % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = '/{}/'.format(TELETUBE_BOT_TOKEN)
