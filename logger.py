import logging
import os

import config


def get_default_handler(filename):
    default_formatter = logging.Formatter(
        "(asctime)s - %(name)s - %(levelname)s: %(message)s"
    )
    filename = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        filename,
        config.LOGS_FOLDER
    )
    handler = logging.FileHandler(filename)
    handler.setFormatter(default_formatter)
    return handler


web_hook_logger = logging.getLogger("SERVER")
web_hook_logger.setLevel(logging.INFO)
web_hook_logger.addHandler(get_default_handler("server.log"))
